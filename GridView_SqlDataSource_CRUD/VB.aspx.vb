﻿
Partial Class VB
    Inherits System.Web.UI.Page
    Protected Sub Insert(sender As Object, e As EventArgs)
        SqlDataSource1.Insert()
    End Sub

    Protected Sub OnRowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow AndAlso GridView1.EditIndex <> e.Row.RowIndex Then
            TryCast(e.Row.Cells(2).Controls(2), LinkButton).Attributes("onclick") = "return confirm('Do you want to delete this row?');"
        End If
    End Sub
End Class
